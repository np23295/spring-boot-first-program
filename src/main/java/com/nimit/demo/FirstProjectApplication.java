package com.nimit.demo;

import javax.xml.ws.RequestWrapper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@EnableAutoConfiguration
public class FirstProjectApplication {
	
	@RequestMapping("/")
	private String home(){
		return "Hello , This is First Sprint Boot Program";	
	}

	public static void main(String[] args) {
		SpringApplication.run(FirstProjectApplication.class, args);
	}
}
